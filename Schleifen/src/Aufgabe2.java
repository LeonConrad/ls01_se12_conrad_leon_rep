import java.util.Scanner;

/**
 * 
 */

/**
 * @author user
 *
 */
public class Aufgabe2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		System.out.println("Aufgabe 2");

		System.out.println("Gebe eine Zahl ein");

		int zahl = tastatur.nextInt();

		int i = 75;
		while (i < 151) {
			System.out.println(i);
			i++;

			if (i % zahl == 1) {
				System.out.println("Die Zahl ist teilbar");
			} else
				System.out.println("Die Zahl ist nicht teilbar");
		}

	}

}
