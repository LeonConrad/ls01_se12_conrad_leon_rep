import java.util.Scanner;

/**
 * 
 */

/**
 * @author user
 *
 */
public class Aufgabe4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		System.out.println("Aufgabe 4");
		System.out.println("Gebe eine Zahl ein");
		int zahl = tastatur.nextInt();
		int summe = 0;

		for (int i = 1; i <= zahl; i = i + 1) {
			summe = summe + i;
		}
		System.out.println("Das Ergebnis ist: " + summe);

	}

}
