import java.util.Scanner;

/**
 * 
 */

/**
 * 
 *
 */
public class Aufgabe3 {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		System.out.println("Aufgabe 3");
		System.out.println("Gebe eine Zahl ein");
		int zahl = tastatur.nextInt();

		for (int i = 1; i <= zahl; i++) {

			if (zahl % i == 0) {
				System.out.println("i = " + i + " ist teilbar");
			}
		}

	}
}
