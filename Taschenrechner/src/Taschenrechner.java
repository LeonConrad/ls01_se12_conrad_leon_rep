import java.util.Scanner;


public class Taschenrechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int zahl1 = 6;
		int zahl2 = 3;
		int ergebnis1 = zahl1 + zahl2;
		int ergebnis2 = zahl1 - zahl2;
		int ergebnis3 = zahl1 * zahl2;
		int ergebnis4 = zahl1 / zahl2;
		
		System.out.println("Zahl1= " + zahl1);
		System.out.println("Zahl2= " + zahl2);
		
		System.out.println("Zahl1 + Zahl2= " + ergebnis1);
		System.out.println("Zahl1 - Zahl2= " + ergebnis2);
		System.out.println("Zahl1 * Zahl2= " + ergebnis3);
		System.out.println("Zahl1 / Zahl2= " + ergebnis4);
		
		Scanner sc = new Scanner(System.in);
		byte zahl;
		
		System.out.println("Was wollen sie rechnen?");
		zahl = sc.nextByte();
		
		System.out.println("Eingabe: " + zahl);
	}

}
