import java.util.Scanner;

public class Verzweigungen {

	public static void main(String[] args) {
		aufgabe5();
	}
	
	public static void aufgabe1() {
		Scanner eingabe = new Scanner(System.in);
		int zahl1, zahl2;
		System.out.println("Geben Sie die erste Zahl ein: ");
		zahl1 = eingabe.nextInt();
		System.out.println("Geben Sie die zweite Zahl ein: ");
		zahl2 = eingabe.nextInt();
		if(zahl1 == zahl2) {
			System.out.println("Die Zahlen sind gleich.");
		}
	}
	
	public static void aufgabe2() {
		Scanner eingabe = new Scanner(System.in);
		double zahl1, zahl2;
		System.out.println("Geben Sie die erste Zahl ein: ");
		zahl1 = eingabe.nextDouble();
		System.out.println("Geben Sie die zweite Zahl ein: ");
		zahl2 = eingabe.nextDouble();
		if(zahl1 == zahl2) {
			System.out.println(zahl1 + " " + zahl2);
		} else {
			if(zahl1 < zahl2) {
				System.out.println(zahl1);
				System.out.println(zahl2);
			} else {
				System.out.println(zahl2);
				System.out.println(zahl1);
			}
		}
	}
	
	public static void aufgabe3() {
		Scanner eingabe = new Scanner(System.in);
		double zahl;
		System.out.println("Geben Sie eine Zahl ein: ");
		zahl = eingabe.nextDouble();
		
		if(zahl >= 0) {
			System.out.println("Die Zahl ist positiv");
		} else {
			System.out.println("Die Zahl ist negativ");
		}
	}
	
	public static void aufgabe4() {
		Scanner eingabe = new Scanner(System.in);
		double zahl;
		System.out.println("Geben Sie eine Zahl zwischen 0 und 100 ein: ");
		zahl = eingabe.nextDouble();
		
		if(zahl >= 0 && zahl <=100) {
			if(zahl > 50) {
				System.out.println("gro�");
			} else {
				System.out.println("klein");
			}
		} else {
			System.out.println("Fehlerhafte Eingabe");
		}
	}
	
	public static void aufgabe5() {
		Scanner eingabe = new Scanner(System.in);
		int zahl;
		System.out.println("Geben Sie eine Zahl ein: ");
		zahl = eingabe.nextInt();
		
		int mod = zahl%2;
		if(mod == 0) {
			System.out.println("gerade");
		} else {
			System.out.println("ungerade");
		}

	}

}
